@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                    <span class="text-uppercase fw-bold">Matriculas</span>
                </div>

                <div class="card-body">
                    @if(session('mensaje'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            {{ session('mensaje') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-6">

                            {{-- <a href="{{ route('inscripciones.estudiante.create') }}" class="btn btn-success text-white">Nuevo</a> --}}

                            {{-- <a href="{{ route('usuarios.exportar.excel') }}" class="btn btn-outline-success">Exportar Excel</a>

                            <a href="{{ route('usuarios.exportar.pdf') }}" class="btn btn-outline-danger">Exportar PDF</a> --}}
                        </div>

                        {{-- <div class="col-md-6">
                            <form action="" method="GET">
                                <div class="input-group mb-3">
                                    <input type="text" name="usuario" class="form-control" autocomplete="off">
                                    <button type="submit" class="btn btn-primary text-white">Buscar</button>
                                </div>
                            </form>
                        </div> --}}

                        {{-- {{$usuario->inscripciones}} --}}

                    </div>

                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Estudiante</th>
                                <th>Programa</th>
                                <th>Semestre</th>
                                <th>Cursos</th>
                                
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($matriculas as $matricula)
                                <tr>
                                    <td>{{ $matricula->inscripcion->user->persona->apepat }} {{ $matricula->inscripcion->user->persona->apemat }} {{ $matricula->inscripcion->user->persona->nombres }}</td>
                                    <td>{{ $matricula->inscripcion->programa->nombre }}</td>
                                    <td>{{ $matricula->inscripcion->semestre->nombre }}</td>
                                    <td>
                                        @foreach ($matricula->cursos as $curso)
                                            {{ $curso->nombre }},
                                        @endforeach
                                    </td>
                                    
                                </tr>
                            @endforeach                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection