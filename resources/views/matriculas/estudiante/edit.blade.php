@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Editar Matrícula
                </div>

                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="card-body">

                    <form action="{{ route('matriculas.estudiante.update', $matricula) }}" method="POST">

                        @method('PUT')

                        @csrf

                        <div class="mb-3">
                            <label for="">Estudiante</label>
                            <input type="text" value="{{ $matricula->inscripcion->user->persona->apepat }} {{ $matricula->inscripcion->user->persona->apemat }}, {{ $matricula->inscripcion->user->persona->nombres }}" class="form-control" autocomplete="off" readonly>
                        </div>

                        <div class="mb-3">
                            <label for="">Programa</label>
                            <input type="text" value="{{ $matricula->inscripcion->programa->nombre }}" class="form-control" autocomplete="off" readonly>
                        </div>

                        <div class="mb-3">
                            <label for="">Cursos</label>
                            <select multiple class="form-select" required readonly>
                                @foreach ($matricula->cursos as $curso)
                                    <option selected value="{{ $curso->id }}">{{ $curso->nombre }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="">Seleccionar Cursos</label>
                            <select name="cursos[]" multiple class="form-select" required>
                                @foreach (App\Models\Curso::where('programa_id', $matricula->inscripcion->programa->id)->get() as $curso)
                                    <option value="{{ $curso->id }}">{{ $curso->nombre }}</option>
                                @endforeach
                            </select>
                        </div>

                        {{-- <div class="mb-3">
                            <label for="">Seleccionar Cursos</label>
                            <select name="cursos[]" multiple class="form-select" required>
                                
                            </select>
                        </div> --}}

                        {{-- <input type="hidden" name="inscripcion_id" value="{{ $inscripcion->id }}">

                        <div class="mb-3">
                            <label for="">Estudiante</label>
                            <input type="text" value="{{ $inscripcion->user->persona->apepat }} {{ $inscripcion->user->persona->apemat }}, {{ $inscripcion->user->persona->nombres }}" class="form-control" autocomplete="off" readonly>
                        </div>

                        <div class="mb-3">
                            <label for="">Programa</label>
                            <input type="text" value="{{ $inscripcion->programa->nombre }}" class="form-control" autocomplete="off" readonly>
                        </div>

                        <div class="mb-3">
                            <label for="">Cursos</label>
                            <select name="cursos[]" multiple class="form-select" required>
                                @foreach (App\Models\Curso::where('programa_id', $inscripcion->programa->id)->get() as $curso)
                                    <option value="{{ $curso->id }}">{{ $curso->nombre }}</option>
                                @endforeach
                            </select>
                        </div> --}}

                        <div class="mb-3">
                            <input type="submit" class="btn btn-warning text-black" value="Guardar">

                            <a href="{{ route('matriculas.estudiante.index') }}" class="btn btn-secondary">Volver</a>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection