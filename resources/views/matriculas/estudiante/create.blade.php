@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Nueva Matrícula
                </div>

                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="card-body">

                    <form action="{{ route('matriculas.estudiante.store') }}" method="POST">

                        @csrf
                        
                        <input type="hidden" name="inscripcion_id" value="{{ $inscripcion->id }}">

                        <div class="mb-3">
                            <label for="">Estudiante</label>
                            <input type="text" value="{{ $inscripcion->user->persona->apepat }} {{ $inscripcion->user->persona->apemat }}, {{ $inscripcion->user->persona->nombres }}" class="form-control" autocomplete="off" readonly>
                        </div>

                        <div class="mb-3">
                            <label for="">Programa</label>
                            <input type="text" value="{{ $inscripcion->programa->nombre }}" class="form-control" autocomplete="off" readonly>
                        </div>

                        <div class="mb-3">
                            <label for="">Semestre</label>
                            <input type="text" name="semestre_id" value="{{ $inscripcion->semestre->nombre }}" class="form-control" autocomplete="off" readonly>
                        </div>

                        <div class="mb-3">
                            <label for="">Cursos</label>
                            <select name="cursos[]" multiple class="form-select" required>
                                @foreach (App\Models\Curso::where('programa_id', $inscripcion->programa->id)->get() as $curso)
                                    <option value="{{ $curso->id }}">{{ $curso->nombre }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3">
                            <input type="submit" class="btn btn-success text-white" value="Guardar">

                            <a href="{{ route('inscripciones.estudiante.index') }}" class="btn btn-secondary">Volver</a>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection