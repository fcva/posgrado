@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                    <span class="text-uppercase fw-bold">Persona</span>
                </div>

                <div class="card-body">
                    @if(session('mensaje'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            {{ session('mensaje') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item"><b>Nombre de Usuario:</b> {{ $usuario->name }}</li>
                        <li class="list-group-item"><b>Email:</b> {{ $usuario->email }}</li>
                        <li class="list-group-item"><b>Nombres:</b> {{ $usuario->persona->nombres }}</li>
                        <li class="list-group-item"><b>Apellido Paterno:</b> {{ $usuario->persona->apepat }}</li>
                        <li class="list-group-item"><b>Apellido Materno:</b> {{ $usuario->persona->apemat }}</li>
                        <li class="list-group-item"><b>DNI:</b> {{ $usuario->persona->docu_num }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection