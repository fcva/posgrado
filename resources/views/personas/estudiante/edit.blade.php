@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Editar Datos
                </div>

                <div class="card-body">
                    <form action="{{ route('personas.estudiante.update') }}" method="POST">

                        @csrf

                        @method('PUT')

                        <input type="hidden" name="user_id" value="{{ $usuario->id }}">

                        <input type="hidden" name="persona_id" value="{{ $usuario->persona->id }}">

                        <div class="mb-3">
                            <label for="">Nombres</label>
                            <input type="text" name="nombres" class="form-control" value="{{ $usuario->persona->nombres }}" autocomplete="off" required>
                        </div>

                        <div class="mb-3">
                            <label for="">Apellido Paterno</label>
                            <input type="text" name="apepat" class="form-control" value="{{ $usuario->persona->apepat }}" autocomplete="off" required>
                        </div>

                        <div class="mb-3">
                            <label for="">Apellido Materno</label>
                            <input type="text" name="apemat" class="form-control" value="{{ $usuario->persona->apemat }}" autocomplete="off" required>
                        </div>

                        <div class="mb-3">
                            <label for="">DNI</label>
                            <input type="text" name="docu_num" class="form-control" value="{{ $usuario->persona->docu_num }}" autocomplete="off" required>
                        </div>

                        {{-- @foreach ($usuario->inscripciones as $inscripcion)
                            @if($inscripcion->programa->nombre == null)
                                <div class="mb-3">
                                    <label for="">Programa</label>
                                    <select name="programa_id" class="form-select" required>
                                        
                                        <option value="">
                                            @foreach ($usuario->inscripciones as $inscripcion)
                                                {{ $inscripcion->programa->nombre }}
                                            @endforeach
                                        </option>
                                                                                                            
                                        <option value="">--- Seleccionar ---</option>
                                        @foreach (App\Models\Programa::get() as $programa)
                                            <option value="{{ $programa->id }}">{{ $programa->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @else
                                @foreach ($usuario->inscripciones as $inscripcion)
                                    <input type="text" value="{{ $inscripcion->programa->nombre }}">
                                @endforeach
                            @endif
                        @endforeach --}}

                        {{-- <div class="mb-3">
                            <label for="">Programa</label>
                            <select name="programa_id" class="form-select" required>
                                
                                <option value="">
                                    @foreach ($usuario->inscripciones as $inscripcion)
                                        {{ $inscripcion->programa->nombre }}
                                    @endforeach
                                </option>
                                                                                                    
                                <option value="">--- Seleccionar ---</option>
                                @foreach (App\Models\Programa::get() as $programa)
                                    <option value="{{ $programa->id }}">{{ $programa->nombre }}</option>
                                @endforeach
                            </select>
                        </div> --}}

                        <div class="mb-3">
                            <input type="submit" class="btn btn-warning" value="Guardar">

                            {{-- <a href="" class="btn btn-secondary">Volver</a> --}}
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection