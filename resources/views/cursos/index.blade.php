@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                    <span class="text-uppercase fw-bold">Cursos</span>
                </div>

                <div class="card-body">
                    @if(session('mensaje'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            {{ session('mensaje') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-6">

                            <a href="{{ route('cursos.create') }}" class="btn btn-success text-white">Nuevo</a>

                            {{-- <a href="{{ route('usuarios.exportar.excel') }}" class="btn btn-outline-success">Exportar Excel</a>

                            <a href="{{ route('usuarios.exportar.pdf') }}" class="btn btn-outline-danger">Exportar PDF</a> --}}
                        </div>

                        <div class="col-md-6">
                            <form action="" method="GET">
                                <div class="input-group mb-3">
                                    <input type="text" name="usuario" class="form-control" autocomplete="off">
                                    <button type="submit" class="btn btn-primary text-white">Buscar</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Curso</th>
                                <th>Programa</th>
                                <th>Docente</th>                                
                                <th>Editar</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cursos as $key => $curso)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $curso->nombre }}</td>
                                    <td>{{ $curso->programa->nombre }}</td>
                                    <td>{{ $curso->user->persona->apepat }} {{ $curso->user->persona->apemat }} {{ $curso->user->persona->nombres }}</td>
                                    <td>
                                        <a href="" class="btn btn-warning">Editar</a>
                                    </td>
                                    <td>
                                        <a href="" class="btn btn-danger text-white">Eliminar</a>                                                 
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection