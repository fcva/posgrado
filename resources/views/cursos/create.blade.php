@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Nuevo Curso
                </div>

                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="card-body">

                    <form action="{{ route('cursos.store') }}" method="POST">

                        @csrf

                        <div class="mb-3">
                            <label for="">Programa</label>
                            <select name="programa_id" class="form-select" required>
                                <option value="">--- Seleccionar ---</option>
                                @foreach (App\Models\Programa::get() as $programa)
                                    <option value="{{ $programa->id }}">{{ $programa->nombre }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="">Docente</label>
                            <select name="user_id" class="form-select" required>
                                <option value="">--- Seleccionar ---</option>
                                @foreach (App\Models\User::get() as $user)
                                    <option value="{{ $user->id }}">
                                        {{ $user->persona->apepat }} {{ $user->persona->apemat }}, {{ $user->persona->nombres }}
                                        --- Rol:
                                        @foreach ($user->roles as $role)
                                            {{ $role->nombre }}
                                        @endforeach
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="">Nombre del curso</label>
                            <input type="text" name="nombre" class="form-control" autocomplete="off" required>
                        </div>

                        <div class="mb-3">
                            <input type="submit" class="btn btn-success text-white" value="Guardar">

                            <a href="" class="btn btn-secondary">Volver</a>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection