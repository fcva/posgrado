@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Editar Semestre
                </div>

                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="card-body">

                    <form action="{{ route('semestres.update', $semestre) }}" method="POST">

                        @method('PUT')

                        @csrf
                        
                        <div class="mb-3">
                            <label for="">Semestre</label>
                            <input type="text" name="nombre" value="{{ $semestre->nombre }}" class="form-control" autocomplete="off" required>
                        </div>

                        <div class="mb-3">
                            <label for="">Cursos</label>
                            <select name="es_activo" class="form-select" required>
                                <option value="{{ $semestre->es_activo }}">
                                    @if ($semestre->es_activo == '1')
                                        ACTIVO
                                    @else
                                        FINALIZADO
                                    @endif
                                </option>
                                <option value="">--- Seleccionar ---</option>
                                <option value="1">ACTIVO</option>
                                <option value="0">FINALIZADO</option>
                            </select>
                        </div>

                        <div class="mb-3">
                            <input type="submit" class="btn btn-warning text-dark" value="Guardar">

                            <a href="{{ route('semestres.index') }}" class="btn btn-secondary">Volver</a>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection