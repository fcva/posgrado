@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Nueva Inscripción
                </div>

                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="card-body">
                    <form action="{{ route('inscripciones.estudiante.store') }}" method="POST">

                        @csrf

                        <div class="mb-3">
                            <label for="">Programa</label>
                            <select name="programa_id" class="form-select" required>
                                <option value="">--- Seleccionar ---</option>
                                @foreach (App\Models\Programa::get() as $programa)
                                    <option value="{{ $programa->id }}">{{ $programa->nombre }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="">Semestre</label>
                            <select name="semestre_id" class="form-select" required>
                                <option value="">--- Seleccionar ---</option>

                                @foreach (App\Models\Semestre::get() as $semestre)
                                    @if ($semestre->es_activo == '1')
                                        <option value="{{ $semestre->id }}">{{ $semestre->nombre }}</option>
                                    @else

                                    @endif
                                @endforeach

                            </select>
                        </div>

                        

                        {{-- <div class="mb-3">
                            <label for="">Rol</label>
                            <select name="rol" class="form-select">
                                <option value="">--- Seleccionar ---</option>
                                <option value="Estudiante">Estudiante</option>
                                <option value="Administrador">Administrador</option>
                            </select>
                        </div> --}}

                        <div class="mb-3">
                            <input type="submit" class="btn btn-success text-white" value="Guardar">

                            <a href="{{ route('inscripciones.estudiante.index') }}" class="btn btn-secondary">Volver</a>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection