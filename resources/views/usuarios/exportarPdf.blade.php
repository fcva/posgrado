<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exportar Usuario</title>


</head>
<body>
    
    <table border="1">
        <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Email</th>
                
                <th>Rol</th>
            </tr>
        </thead>
        <tbody>
            @foreach($usuarios as $key => $usuario)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td width="30%">{{ $usuario->name }}</td>
                    <td width="30%">{{ $usuario->email }}</td>
                    
                    <td width="30%">{{ $usuario->rol }}</td>         
                </tr>
            @endforeach
        </tbody>
    </table>

</body>
</html>