@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Nuevo Usuario
                </div>

                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            @foreach($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="card-body">
                    <form action="{{ route('usuarios.store') }}" method="POST">

                        @csrf

                        <div class="mb-3">
                            <label for="">Nombre</label>
                            <input type="text" name="name" class="form-control" autocomplete="off" required>
                        </div>

                        <div class="mb-3">
                            <label for="">Email</label>
                            <input type="email" name="email" class="form-control" autocomplete="off" required>
                        </div>

                        <div class="mb-3">
                            <label for="">Rol</label>
                            <select name="rol" class="form-select">
                                <option value="">--- Seleccionar ---</option>
                                <option value="Estudiante">Estudiante</option>
                                <option value="Administrador">Administrador</option>
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" autocomplete="off" required>
                        </div>

                        <div class="mb-3">
                            <input type="submit" class="btn btn-success text-white" value="Guardar">

                            <a href="{{ route('usuarios.index') }}" class="btn btn-secondary">Volver</a>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection