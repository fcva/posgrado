@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Eliminar Usuarios
                </div>

                <div class="card-body">

                    <form action="{{ route('usuarios.delete', $usuario) }}" method="POST">

                        @csrf

                        @method('DELETE')

                        <div class="mb-3">
                            <label for="">Nombre</label>
                            <input type="text" name="name" class="form-control" value="{{ $usuario->name }}" autocomplete="off" required>
                        </div>

                        <div class="mb-3">
                            <label for="">Email</label>
                            <input type="email" name="email" class="form-control" value="{{ $usuario->email }}" autocomplete="off" required>
                        </div>

                        <div class="mb-3">
                            <label for="">Rol</label>
                            <select name="rol" class="form-select">
                                <option value="{{ $usuario->rol }}">{{ $usuario->rol }}</option>
                                <option value="">--- Seleccionar ---</option>
                                <option value="Estudiante">Estudiante</option>
                                <option value="Administrador">Administrador</option>
                            </select>
                        </div>

                        <div class="mb-3">
                            <input type="submit" class="btn btn-danger text-white" value="Eliminar">

                            <a href="{{ route('usuarios.index') }}" class="btn btn-secondary">Volver</a>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection