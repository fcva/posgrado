@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                    <span class="text-uppercase fw-bold">Usuarios</span>
                </div>

                <div class="card-body">
                    @if(session('mensaje'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            {{ session('mensaje') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-6">

                                <a href="{{ route('usuarios.create') }}" class="btn btn-success text-white">Nuevo</a>

                            <a href="{{ route('usuarios.exportar.excel') }}" class="btn btn-outline-success">Exportar Excel</a>

                            <a href="{{ route('usuarios.exportar.pdf') }}" class="btn btn-outline-danger">Exportar PDF</a>
                        </div>

                        <div class="col-md-6">
                            <form action="{{ route('usuarios.index') }}" method="GET">
                                <div class="input-group mb-3">
                                    <input type="text" name="usuario" class="form-control" autocomplete="off">
                                    <button type="submit" class="btn btn-primary text-white">Buscar</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Usuario</th>
                                <th>Email</th>
                                <th>Persona</th>
                                <th>Rol</th>
                                <th>editar</th>
                                <th>eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $key => $usuario)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $usuario->name }}</td>
                                    <td>{{ $usuario->email }}</td>
                                    <td>
                                        {{ $usuario->persona->apepat }} {{ $usuario->persona->apemat }}, {{ $usuario->persona->nombres }}
                                    </td>
                                    <td>
                                        @foreach ($usuario->roles as $role)
                                            {{ $role->nombre }}
                                        @endforeach
                                    </td>
                                    <td>
                                            <a href="{{ route('usuarios.edit', $usuario->id) }}" class="btn btn-warning">Editar</a>
                                    </td>
                                    <td>
                                            <a href="{{ route('usuarios.destroy', $usuario->id) }}" class="btn btn-danger text-white">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
