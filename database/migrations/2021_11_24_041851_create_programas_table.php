<?php

use App\Models\Programa;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('descripcion')->nullable();
            $table->boolean('es_activo')->default(true);
            $table->timestamps();
        });

        Programa::create([
            'nombre' => 'Administración de Empresas'
        ]);

        Programa::create([
            'nombre' => 'Ingeniería Informática'
        ]);

        Programa::create([
            'nombre' => 'Enfermería'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programas');
    }
}
