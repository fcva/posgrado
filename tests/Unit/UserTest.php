<?php

namespace Tests\Unit;

use App\Models\{User,Role};
use Illuminate\Support\Facades\{Hash,Schema};
//use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function test_validar_campos_en_tabla_users()
    {
        $this->assertTrue(
            Schema::hasColumns('users', ['id', 'name', 'email'], 1)
        );
    }

    public function test_crear_usuario_con_relacion_a_persona()
    {
        $user = User::create([
            'name' => 'Invitado',
            'email' => 'invitado@gmail.com',
            'password' => Hash::make('12345678')
        ]);

        $user->persona()->create();

        $role = Role::find(2);

        $user->roles()->attach($role);

        $this->assertInstanceOf(User::class, $user);
        
    }

    public function test_filtrar_usuario_por_id()
    {
        $user = User::find(1);

        $this->assertInstanceOf(User::class, $user);
    }
}
