<?php

namespace App\Providers;

use App\Models\Permiso;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class PermisosServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /*Permiso::get()->map(function($permiso) {

            Gate::define($permiso->nombre, function($user) use ($permiso) {

                return $user->tienePermiso($permiso);
            });
        });*/
    }
}
