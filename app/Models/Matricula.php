<?php

namespace App\Models;

use App\Models\{Inscripcion,Curso};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Relación inversa de uno a muchos con Inscripcion 
     */
    public function inscripcion()
    {
        return $this->belongsTo(Inscripcion::class, 'inscripcion_id');
    }

    /**
     * Realación de muchos a muchos con Curso
     */
    public function cursos()
    {
        return $this->belongsToMany(Curso::class, 'matricula_curso');
    }
}
