<?php

namespace App\Models;

use App\Models\{User,Programa,Semestre,Matricula};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inscripcion extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Relación inversa de uno a muchos con User 
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Relación inversa de uno a muchos con User 
     */
    public function programa()
    {
        return $this->belongsTo(Programa::class, 'programa_id');
    }

    /**
     * Relación inversa de uno a muchos con Semestre 
     */
    public function semestre()
    {
        return $this->belongsTo(Semestre::class, 'semestre_id');
    }

    /**
     * Relacion de uno a muchos con Matricula
     */
    public function matriculas()
    {
        return $this->hasMany(Matricula::class);
    }
}
