<?php

namespace App\Models;

use App\Models\{Matricula,User,Programa};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Realación de muchos a muchos con Curso
     */
    public function matriculas()
    {
        return $this->belongsToMany(Matricula::class, 'matricula_curso');
    }

    /**
     * Relación inversa de uno a muchos con User 
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Relación inversa de uno a muchos con Programa 
     */
    public function programa()
    {
        return $this->belongsTo(Programa::class, 'programa_id');
    }
}
