<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Relacion de uno a uno con el modelo User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
