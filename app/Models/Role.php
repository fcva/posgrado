<?php

namespace App\Models;

use App\Models\{User,Permiso};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Realación de muchos a muchos con User
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'users_roles');
    }

    /**
     * Realación de muchos a muchos con Permiso
     */
    public function permisos()
    {
        return $this->belongsToMany(Permiso::class, 'roles_permisos');
    }
}
