<?php

namespace App\Models;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Realación de muchos a muchos con Role
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'roles_permisos');
    }
}
