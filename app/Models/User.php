<?php

namespace App\Models;

use App\Models\{Persona,Role,Inscripcion,Curso};
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\{SoftDeletes,Builder};

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'rol'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeFiltrar(Builder $builder, $usuario)
    {
        if(trim($usuario) != '') {

            $builder->where('name', 'LIKE', "%$usuario%")->orWhere('email', 'LIKE', "%$usuario%");
        }
    }

    /**
     * Relación de uno a uno con Persona
     */
    public function persona()
    {
        return $this->hasOne(Persona::class);
    }

    /**
     * Relacion de uno a muchos con Inscripcion
     */
    public function inscripciones()
    {
        return $this->hasMany(Inscripcion::class);
    }

    /**
     * Realación de muchos a muchos con Role
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_roles');
    }

    /**
     * Relacion de uno a muchos con Curso
     */
    public function cursos()
    {
        return $this->hasMany(Curso::class);
    }


    /**
     * Tiene roles
     */
    public function hasRole(...$roles)
    {
        foreach($roles as $role) {

            if($this->roles->contains('nombre', $role)) {

                return true;
            }
        }

        return false;
    }

    /**
     * Tine permiso a
     */
    public function tienePermiso($permiso)
    {
        return $this->tienePermisoAtravesDeRole($permiso);
    }

    /**
     * Tiene permisos a través de roles
     */
    protected function tienePermisoAtravesDeRole($permiso)
    {
        foreach($permiso->roles as $role)
        {
            if($this->roles->contains($role))
            {
                return true;
            }
        }

        return false;
    }
}
