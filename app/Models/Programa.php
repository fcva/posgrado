<?php

namespace App\Models;

use App\Models\{Inscripcion,Curso};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Programa extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Relacion de uno a muchos con Inscripcion
     */
    public function inscripciones()
    {
        return $this->hasMany(Inscripcion::class);
    }

    /**
     * Relacion de uno a muchos con Curso
     */
    public function cursos()
    {
        return $this->hasMany(Curso::class);
    }
}
