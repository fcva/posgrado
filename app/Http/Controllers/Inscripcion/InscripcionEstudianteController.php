<?php

namespace App\Http\Controllers\Inscripcion;

use App\Models\{User,Inscripcion, Semestre};
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class InscripcionEstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()) {

            $id = auth()->user()->id;

            $usuario = User::find($id);

            return view('inscripciones.estudiante.index', compact('usuario'));
        
        } else {

            return redirect()->route('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inscripciones.estudiante.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add([
            'user_id' => auth()->user()->id
        ]);

        /* $semestre = Semestre::find($request->semestre_id);

        #return [$semestre->nombre, $semestre->id];

        $inscripciones = Inscripcion::get();

        foreach($inscripciones as $inscripcion) {

            if($inscripcion->semestre_id == $semestre->id) {

                #return 'es igual';

                return redirect()->route('inscripciones.estudiante.index')->with('mensaje', 'Los datos ya han sido ingresados.');

            } else {

                #return 'no es igual';
                #return redirect()->route('inscripciones.estudiante.index')->with('mensaje', 'Puedes matricularte a otro semestre.');
                
                Inscripcion::create($request->all());

                return redirect()->route('inscripciones.estudiante.index')->with('mensaje', 'Los datos se han guardado correctamente.');
            }
        } */

        Inscripcion::create($request->all());

        return redirect()->route('inscripciones.estudiante.index')->with('mensaje', 'Los datos se han guardado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
