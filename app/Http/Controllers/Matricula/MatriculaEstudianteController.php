<?php

namespace App\Http\Controllers\Matricula;

use App\Models\{Inscripcion,Curso,Matricula,User,Semestre};
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MatriculaEstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* $matriculas = Matricula::get(); */

        if(Auth::check()) {

            $id = auth()->user()->id;

            $usuario = User::find($id);

            return view('matriculas.estudiante.index', compact('usuario'));
        
        } else {

            return redirect()->route('login');
        }

        /* return view('matriculas.estudiante.index', compact('matriculas')); */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $inscripcion_id = $id;

        $inscripcion = Inscripcion::find($inscripcion_id);

        return view('matriculas.estudiante.create', compact('inscripcion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        /* $semestre_id = Semestre::where('nombre', $request->semestre_id)->value('id');

        $semestre = Semestre::find($semestre_id);

        $inscripcion_id = Inscripcion::where('semestre_id', $semestre_id)->value('id');

        $inscripcion = Inscripcion::find($inscripcion_id);

        if($semestre->id == $inscripcion->semestre_id) {

            return 'igual';

        } else {

            return 'no es igual';
        } */

        /* foreach($matriculas as $matri) {

            if($matri->inscripcion->semestre->nombre == $semestre->id) {

                return redirect()->route('matriculas.estudiante.index')->with('mensaje', 'Los datos ya han sido ingresados.');

            } else {

                #$curso = Curso::find($request->cursos);

                #$matricula = Matricula::create($request->except('cursos','semestre_id'));

                #$matricula->cursos()->attach($curso);
        
                return redirect()->route('matriculas.estudiante.index')->with('mensaje', 'Los datos se han guardado correctamente.');
            }
        } */

        



        $curso = Curso::find($request->cursos);

        $matricula = Matricula::create($request->except('cursos','semestre_id'));

        $matricula->cursos()->attach($curso);
        
        return redirect()->route('matriculas.estudiante.index')->with('mensaje', 'Los datos se han guardado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $matricula = Matricula::find($id);

        return view('matriculas.estudiante.edit', compact('matricula'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Matricula $matricula)
    {
        $curso = Curso::find($request->cursos);

        $matricula->update($request->except('cursos'));

        $matricula->with('cursos')->find($matricula->id)->cursos()->sync($curso);

        return redirect()->route('matriculas.estudiante.index')->with('mensaje', 'Los datos se han guardado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
