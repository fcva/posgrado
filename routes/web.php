<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Semestre\SemestreController;
use App\Http\Controllers\Curso\CursoController;
use App\Http\Controllers\Matricula\{MatriculaEstudianteController,MatriculaController};
use App\Http\Controllers\Inscripcion\InscripcionEstudianteController;
use App\Http\Controllers\Role\RoleController;
use App\Http\Controllers\Permiso\PermisoController;
use App\Http\Controllers\Usuario\UsuarioController;
use App\Http\Controllers\Persona\{EstudianteController,PersonaController};
use Illuminate\Http\Request;

Route::get('/', function (Request $request) {

    $user = $request->user();

    //dd($user);

    //return $user->can('create');

    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/**
 * User
 */
Route::get('/usuarios/index', [UsuarioController::class, 'index'])->name('usuarios.index')->middleware('auth');
Route::get('/usuarios/create', [UsuarioController::class, 'create'])->name('usuarios.create');
Route::post('/usuarios/store', [UsuarioController::class, 'store'])->name('usuarios.store');
Route::get('/usuarios/edit/{id}', [UsuarioController::class, 'edit'])->name('usuarios.edit');
Route::put('/usuarios/update/{usuario}', [UsuarioController::class, 'update'])->name('usuarios.update');
Route::get('/usuarios/destroy/{id}', [UsuarioController::class, 'destroy'])->name('usuarios.destroy');
Route::delete('/usuarios/delete/{usuario}', [UsuarioController::class, 'delete'])->name('usuarios.delete');
Route::get('/usuarios/exportar/excel', [UsuarioController::class, 'exportarExcel'])->name('usuarios.exportar.excel');
Route::get('/usuarios/exportar/pdf', [UsuarioController::class, 'exportarPdf'])->name('usuarios.exportar.pdf');

/**
 * Persona
 */
Route::get('/personas/index', [PersonaController::class, 'index'])->name('personas.index');

Route::get('/personas/estudiante/index', [EstudianteController::class, 'index'])->name('personas.estudiante.index');
Route::get('/personas/estudiante/edit', [EstudianteController::class, 'edit'])->name('personas.estudiante.edit');
Route::put('/personas/estudiante/update', [EstudianteController::class, 'update'])->name('personas.estudiante.update');

/**
 * Permiso
 */
Route::get('/permisos/index', [PermisoController::class, 'index'])->name('permisos.index');

/**
 * Role
 */
Route::get('/roles/index', [RoleController::class, 'index'])->name('roles.index');

/**
 * Inscripcion
 */
Route::get('/inscripciones/estudiante/index', [InscripcionEstudianteController::class, 'index'])->name('inscripciones.estudiante.index');
Route::get('/inscripciones/estudiante/create', [InscripcionEstudianteController::class, 'create'])->name('inscripciones.estudiante.create');
Route::post('/inscripciones/estudiante/store', [InscripcionEstudianteController::class, 'store'])->name('inscripciones.estudiante.store');

/**
 * Matricula
 */
Route::get('/matriculas/index', [MatriculaController::class, 'index'])->name('matriculas.index');

Route::get('/matriculas/estudiante/index', [MatriculaEstudianteController::class, 'index'])->name('matriculas.estudiante.index');
Route::get('/matriculas/estudiante/create/{id}', [MatriculaEstudianteController::class, 'create'])->name('matriculas.estudiante.create');
Route::post('/matriculas/estudiante/store', [MatriculaEstudianteController::class, 'store'])->name('matriculas.estudiante.store');
Route::get('/matriculas/estudiante/edit/{id}', [MatriculaEstudianteController::class, 'edit'])->name('matriculas.estudiante.edit');
Route::put('/matriculas/estudiante/update/{matricula}', [MatriculaEstudianteController::class, 'update'])->name('matriculas.estudiante.update');

/**
 * Curso
 */
Route::get('/cursos/index', [CursoController::class, 'index'])->name('cursos.index');
Route::get('/cursos/create', [CursoController::class, 'create'])->name('cursos.create');
Route::post('/cursos/store', [CursoController::class, 'store'])->name('cursos.store');

/**
 * Semestre
 */
Route::get('/semestres/index', [SemestreController::class, 'index'])->name('semestres.index');
Route::get('/semestres/create', [SemestreController::class, 'create'])->name('semestres.create');
Route::post('/semestres/store', [SemestreController::class, 'store'])->name('semestres.store');
Route::get('/semestres/edit/{id}', [SemestreController::class, 'edit'])->name('semestres.edit');
Route::put('/semestres/update/{semestre}', [SemestreController::class, 'update'])->name('semestres.update');